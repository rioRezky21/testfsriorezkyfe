import React, { useEffect, useState } from 'react'
import {
  CModal,
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CSpinner,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CFormGroup,
  CLabel,
  CInput,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CForm
} from '@coreui/react'

import axios from 'axios'

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}
const fields = ['id', 'name', 'username']

const Dashboard = () => {
  const [getAllData, setGetAllData] = useState(true)
  const [createUser, setCreateUser] = useState(false)
  const [deleteUser, setDeleteUser] = useState(false)
  const [dataUsers, setDataUsers] = useState([])
  const [createPopup, setCreatePopup] = useState(false)
  const [deletePopup, setDeletePopup] = useState(false)
  const [inputName, setInputName] = useState('')
  const [inputPassword, setInputPassword] = useState('')
  const [inputPasswordMatch, setInputPasswordMatch] = useState('')
  const [inputUsername, setInputUsername] = useState('')
  const [inputUserId, setInputUserId] = useState('')

  // FORM VALIDATION
  const [isNameValid, setIsNameValid] = useState('')
  const [isUsernameValid, setIsUsernameValid] = useState(false)
  const [isPasswordValid, setIsPasswordValid] = useState(false)
  const [isPasswordMatch, setIsPasswordMatch] = useState(false)
  const [isSubmitable, setIsSubmitable] = useState(false)
  const [isDelete, setIsDelete] = useState(false)

  useEffect(() => {

    // GET ALL DATA
    if (getAllData) {
      const urlGet = `https://sharingvision-backend.herokuapp.com/user`
      axios.get(urlGet).then(res => {
        setDataUsers(res.data.data)

        if (dataUsers.length === res.data.data.length) {
          setGetAllData(false)
        }

      })
    }

    // Create User
    if (createUser) {
      const urlPost = `https://sharingvision-backend.herokuapp.com/user`
      const body = {
        name: inputName,
        password: inputPassword,
        username: inputUsername
      }
      axios.post(urlPost, body)
        .then(res => {
          if (res.status === 200) {
            setCreateUser(false)
            setGetAllData(true)
            setCreatePopup(!createPopup)
          }
        })
    }

    // Delete User
    if (deleteUser) {
      const urlDelete = `https://sharingvision-backend.herokuapp.com/user/` + inputUserId

      axios.delete(urlDelete)
        .then(res => {
          if (res.status === 200) {
            setDeleteUser(false)
            setGetAllData(true)
            setDeletePopup(!deletePopup)
          }
        })
    }

    // CREATE VALIDATION
    if (inputName.length > 3) {
      setIsNameValid(true)
    } else {
      setIsNameValid(false)
    }

    if (inputUsername.length > 3) {
      setIsUsernameValid(true)
    } else {
      setIsUsernameValid(false)
    }

    if (inputPassword.length > 7) {
      setIsPasswordValid(true)
    } else {
      setIsPasswordValid(false)
    }

    if (inputPassword === inputPasswordMatch && inputPasswordMatch.length !== 0) {
      setIsPasswordMatch(true)
    } else {
      setIsPasswordMatch(false)
    }

    if (isNameValid && isUsernameValid && isPasswordValid && isPasswordMatch) {
      setIsSubmitable(true)
    } else {
      setIsSubmitable(false)
    }

    // DELETE VALIDATION
    if (inputUserId.length > 0) {
      setIsDelete(true)
    } else {
      setIsDelete(false)
    }

  })
  return (
    <>
      {getAllData ?
        <CSpinner
          color="grey"
          style={{
            width: '5rem', height: '5rem', position: 'absolute', left: '50%', top: '50%'
          }}
        />
        :
        <CRow>
          <CCol xs="12" lg="12">
            <CCard>
              <CCardHeader>
                <CRow>
                  <CButton color="primary" size="lg" style={{ marginLeft: "15px" }} onClick={() => setCreatePopup(!createPopup)}>Create User</CButton>
                  <CButton color="danger" size="lg" style={{ marginLeft: "20px" }} onClick={() => setDeletePopup(!deletePopup)}>Delete User</CButton>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={dataUsers}
                  fields={fields}
                  itemsPerPageSelect
                  tableFilter
                  itemsPerPage={5}
                  pagination
                  sorter
                  hover
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      }

      {/* MODAL CREATE USER */}
      <CModal
        show={createPopup}
        onClose={() => setCreatePopup(!createPopup)}
        size="lg"
      >
        <CModalHeader closeButton>
          <CModalTitle>
            <h2>Create User</h2>
          </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CCol xs="12">
            <CFormGroup>
              <CLabel htmlFor="username">Name</CLabel>
              {isNameValid ?
                <CInput valid id="username" placeholder="Enter your name" required onChange={(e) => setInputName(e.target.value)} />
                :
                <CInput invalid id="username" placeholder="Enter your name" required onChange={(e) => setInputName(e.target.value)} />
              }
              <CInvalidFeedback className="help-block">
                username must be 3 characters or more
                  </CInvalidFeedback>
            </CFormGroup>
            <CFormGroup>
              <CLabel htmlFor="username">Username</CLabel>
              {isUsernameValid ?
                <CInput valid id="username" placeholder="Enter your username" required onChange={(e) => setInputUsername(e.target.value)} />
                :
                <CInput invalid id="username" placeholder="Enter your username" required onChange={(e) => setInputUsername(e.target.value)} />
              }
              <CInvalidFeedback className="help-block">
                username must be 3 characters or more
                  </CInvalidFeedback>
            </CFormGroup>
            <CFormGroup>
              <CLabel htmlFor="password">Password</CLabel>
              {isPasswordValid ?
                <CInput valid type="password" id="password" placeholder="Enter your password" onChange={(e) => setInputPassword(e.target.value)} />
                :
                <CInput invalid type="password" id="password" placeholder="Enter your password" onChange={(e) => setInputPassword(e.target.value)} />
              }
              <CInvalidFeedback className="help-block">
                password must be 7 characters or more
                  </CInvalidFeedback>
            </CFormGroup>
            <CFormGroup>
              <CLabel htmlFor="re-password">Re-Enter Password</CLabel>
              {isPasswordMatch ?
                <CInput valid type="password" id="re-password" placeholder="Re-nter your password" onChange={(e) => setInputPasswordMatch(e.target.value)} />
                :
                <CInput invalid type="password" id="re-password" placeholder="Re-nter your password" onChange={(e) => setInputPasswordMatch(e.target.value)} />
              }
              <CInvalidFeedback className="help-block">
                password did not match
                  </CInvalidFeedback>
            </CFormGroup>
          </CCol>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setCreatePopup(!createPopup)}>Cancel</CButton>
          {isSubmitable ?
            <CButton color="primary" onClick={() => setCreateUser(true)}>Submit</CButton>
            :
            <CButton color="dark" style={{ cursor: "not-allowed" }} disabled>Submit</CButton>
          }
        </CModalFooter>
      </CModal>

      {/* DELETE MODAL */}
      <CModal
        show={deletePopup}
        onClose={() => setDeletePopup(!deletePopup)}
        size="md"
      >
        <CModalHeader closeButton>
          <CModalTitle>
            <h2>Delete User</h2>
          </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CCol xs="12">
            <CFormGroup>
              <CLabel htmlFor="userId">User ID</CLabel>
              {isDelete ?
                <CInput valid id="userId" placeholder="Enter User ID" required onChange={(e) => setInputUserId(e.target.value)} />
                :
                <CInput invalid id="userId" placeholder="Enter User ID" required onChange={(e) => setInputUserId(e.target.value)} />
              }
              <CInvalidFeedback className="help-block">
                user ID required
                  </CInvalidFeedback>
            </CFormGroup>
          </CCol>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setDeletePopup(!deletePopup)}>Cancel</CButton>
          {isDelete ?
            <CButton color="danger" onClick={() => setDeleteUser(true)}>Delete</CButton>
            :
            <CButton color="danger" style={{ cursor: "not-allowed" }} disabled>Delete</CButton>
          }
        </CModalFooter>
      </CModal>
    </>
  )
}

export default Dashboard
